import { Component } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { CurrencyService } from './currency.service';
import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';

interface TempObject {
  date: string;
  usd: number;
  eur: number;
  gbp: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  public startDate = new FormControl(); // value from start date field
  public endDate = new FormControl(); // value from end date field
  public dataSource = []; // main array to display search results in table
  public toggleFlag = true; // flag for sorting data in table
  displayedColumns: string[] = ['date', 'eur', 'gbp', 'usd']; // table column caption names

  constructor(
    private currencyService: CurrencyService,
    private datePipe: DatePipe,
    private dateAdapter: DateAdapter<Date>
  ) { this.dateAdapter.setLocale('fr'); }

  // Method to get currency rate info from server
  getInfo(): boolean {
    const dateArray = []; // Collection of dates between start date and end date
    for (let i = Date.parse(this.startDate.value); i <= Date.parse(this.endDate.value); i = i + 24 * 60 * 60 * 1000) {
      dateArray.push(this.datePipe.transform(i, 'yyyyMMdd'));
    }
    if (dateArray.length > 5) {
      alert('Выбранный диапазон не должен превышать 5 дней');
      return false;
    } else if (Date.parse(this.startDate.value) > Date.parse(this.endDate.value)) {
      alert('Начальная дата должна быть меньше конечной');
      return false;
    } else if (!!this.startDate.value && !!this.endDate.value) {
      this.currencyService.getCurrencyValues(dateArray)
        .subscribe(resp => {
          if (!!resp[0][0].message) {
            alert(resp[0][0].message);
            return false;
          }
          const totalArray = [];
          for (const item of resp) {
            if (item.length) {
              const obj: TempObject = { date: null, usd: null, eur: null, gbp: null };
              for (const spec of item) {
                obj.date = spec.exchangedate;
                if (spec.r030 === 840) {
                  obj.usd = Math.round(spec.rate * 100) / 100;
                } else if (spec.r030 === 978) {
                  obj.eur = Math.round(spec.rate * 100) / 100;
                } else if (spec.r030 === 826) {
                  obj.gbp = Math.round(spec.rate * 100) / 100;
                }
              }
              totalArray.push(obj);
            }
          }
          this.dataSource = totalArray;
        },
          (error) => {
            console.log(error);
            alert(error);
          }
        );
    }
  }

  // Method for sorting data in table
  sortData(currency: string): void {
    if (this.dataSource.length) {
      this.toggleFlag = !this.toggleFlag;
      const copy = JSON.parse(JSON.stringify(this.dataSource));
      if (currency === 'usd') {
        this.toggleFlag ? copy.sort((a, b) => a.usd - b.usd) : copy.sort((a, b) => b.usd - a.usd);
      } else if (currency === 'eur') {
        this.toggleFlag ? copy.sort((a, b) => a.eur - b.eur) : copy.sort((a, b) => b.eur - a.eur);
      } else if (currency === 'gbp') {
        this.toggleFlag ? copy.sort((a, b) => a.gbp - b.gbp) : copy.sort((a, b) => b.gbp - a.gbp);
      } else if (currency === 'date') {
        this.toggleFlag ? copy.sort((a, b) => {
          const dateA: any = new Date(a.date);
          const dateB: any = new Date(b.date);
          return dateA - dateB;
        }) : copy.sort((a, b) => {
          const dateA: any = new Date(a.date);
          const dateB: any = new Date(b.date);
          return dateB - dateA;
        });
      }
      this.dataSource = copy;
    }
  }
}
