import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CurrencyService {
  apiUrl = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=';
  constructor(
    private http: HttpClient
  ) { }

  getCurrencyValues(date) {
    const collectData = [];
    date.forEach((item) => {
      collectData.push(this.http.get(`${this.apiUrl}${item}&json`));
    });
    return forkJoin(collectData);
  }

}
